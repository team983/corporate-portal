class ApplicationController < ActionController::Base
  http_basic_authenticate_with name: "login", password: "password"

  before_action :current_user

  def current_user
    @current_user ||= User.first
  end
end
