class User < ApplicationRecord
  has_secure_password

  has_one_attached :avatar
  has_many :created_tasks,  class_name: "Task", foreign_key: :author_id
  has_many :assigned_tasks,  class_name: "Task", foreign_key: :assignee_id

  validates :first_name, :last_name, :middle_name, presence: true
  validates :login, presence: true
  validates :birth_date, presence: true
  validates :password, presence: true, confirmation: true
  validates :password_confirmation, presence: true, allow_blank: false
end
