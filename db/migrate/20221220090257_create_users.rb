class CreateUsers < ActiveRecord::Migration[6.1]
  def change
    create_table :users do |t|
      t.string :last_name, null: false
      t.string :first_name, null: false
      t.string :middle_name, null: false
      t.string :login, null: false
      t.date :birth_date, null: false

      t.timestamps
    end
  end
end
