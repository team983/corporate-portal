class CreateTasks < ActiveRecord::Migration[6.1]
  def change
    create_table :tasks do |t|
      t.string :title, null: false
      t.text :description
      t.integer :status, null: false
      t.references :author, foreign_key: { to_table: :users }, null: false
      t.references :assignee, foreign_key: { to_table: :users }
      t.timestamps
    end
  end
end
